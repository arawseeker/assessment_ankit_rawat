
# README #

This repository has two folders

1. cloudformationTemplate : 
      The Template to create the following resources
      
	    - t3.large EC2 instance
  	    - Security group for the EC2 instance
  	    - A RDS MySQL isntance for JIRA
        
2. Ansible_code

  	To install JIRA on the EC2 instance created by the CloudFormation template

**> Limitations:** 

1.	This template was developed to work with _Amazon Linux_ Platform only 

2. CFN template creates a RDS Mysql resource and only installs Jira as well as mysql on the instance but does not setup/launch Jira.

### Parameters for running the Cloudformation template ###

   **TEMPLATE FILE:  ./cloudformationTemplate/template.yml**

    - KeyName:
    
        Description: Name of an existing EC2 KeyPair to enable SSH access to teh instance
        Default: testKeyAtlassian
        Type: AWS::EC2::KeyPair::KeyName
        ConstraintDescription: must be the name of an existing EC2 Key Pair
    
    - InstanceType: 
    
        Description: Using default instance type as t3.large
        Type: String
        Default: t3.large
        AllowedValues:
        - t3.small
        - t3.medium
        - t3.large
        - t3.xlarge
        - t3.2xlarge
    
    - VpcIdParam:
    
        Description: VPS to launch instance into
        Default: 'vpc-b21886ca'
        Type: AWS::EC2::VPC::Id
    
    - AmiId:
    
        Description: AMI ID for the image to be used for the EC2 Instance
        Default: ami-04590e7389a6e577c
        Type: 'String'
        
    - DBName:
    
        Default: JiraDB
        Description: The database name
        Type: String
    * DBUser:
    
        Default: admin
        NoEcho: 'true'
        Description: The database admin account username
        Type: String
    - DBPassword:
    
        Default: password
        NoEcho: 'true'
        Description: The database admin account password
        Type: String
		
##### *NOTE: The current defaults are the values used for my environment.*  ####
_Please pass the required parameters while testing the CloudFormation template in your environment_

### Ansible Playbook ###

**File: ./Ansible_code/jira.yml**

Currently the playbook only downloads the installer for Jira Core and then runs the installer.
It is configured to replace the server.xml from the template. Thus, if required a custom Domain Name can be configured for JIRA by changing the template **./Ansible_code/roles/jiraInstall/templates/jira_server.xml.j2**. 

(_Please note that variables can be initialized but currently the template static and not configured to use the variables for changing the domain name as per the variables_)

### Running the Ansible Playbook ###

(Automated using CFN template)

The CloudFormation template bootstraps the EC2 instance to install Ansible and other requried dependencies.
Thus, the code is imported and executed automatically using the cloudformation template.
